<?php

namespace Tests;

use App\Call;
use App\Contact;
use App\Interfaces\CarrierInterface;
use App\Interfaces\ContactRepositoryInterface;
use App\Mobile;
use App\Repositories\ContactRepository;
use App\Services\CallerService;
use App\Services\ContactService;
use App\Services\SmsService;
use App\Sms;
use Mockery;
use PHPUnit\Framework\TestCase;

class MobileTest extends TestCase
{

	/** 
	 * @test 
	 * */
	public function it_returns_null_when_name_empty()
	{
		$contactRepository = Mockery::mock(ContactRepositoryInterface::class);

		$provider = Mockery::mock(CarrierInterface::class);
		$provider->allows()->dial();

		$contactService = new ContactService($contactRepository);
		$callerService = new CallerService($provider);
		$smsService = new SmsService($provider);

		$mobile = new Mobile($contactService, $callerService, $smsService);

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** 
	 * @test 
	 * 
	 * Add the necessary code in the production code to check when the contact is not found and add another test to test that case
	 * 
	 * Esto se puede manejar de 2 posibles maneras. Se puede crear mock del resultado y simular el llamado a la base de datos o,
	 * para el caso de Laravel, se podría usar https://laravel.com/docs/8.x/database-testing con un ambiente específico para pruebas.
	 * 
	 * En este caso, opté por el mock.
	 * 
	 * */
	public function it_returns_null_when_contact_not_found()
	{
		$contactRepository = new ContactRepository();

		// Contact not found
		$contact = $contactRepository->findByName('andree');
		$this->assertNull($contact);

		// Contact found
		$contact = $contactRepository->findByName('pedro');
		$this->assertInstanceOf(Contact::class, $contact);
		$this->assertEquals('999999993', $contact->phoneNumber());
	}

	/** 
	 * @test 
	 * 
	 * Add a test for the method makeCallByName passing a valid contact, mock up any hard dependency and add the right assertions
	 * */
	public function it_makes_a_phone_call_when_contact_exists()
	{
		$contact = new Contact('andree', '999999999');

		$contactRepository = Mockery::mock(ContactRepositoryInterface::class);
		$contactRepository->allows()->findByName('andree')->andReturns($contact);

		$provider = Mockery::mock(CarrierInterface::class);
		$provider->allows()->dial()->andReturns("Dialing {$contact->name()} from provider one");
		$provider->allows()->call()->andReturns(new Call($contact));

		$contactService = new ContactService($contactRepository);
		$callerService = new CallerService($provider);
		$smsService = new SmsService($provider);

		$mobile = new Mobile($contactService, $callerService, $smsService);

		$this->assertInstanceOf(Call::class, $mobile->makeCallByName('andree'));
		$this->assertEquals('Calling to contact andree to phone number 999999999', $mobile->makeCallByName('andree')->start());
	}

	/** 
	 * @test 
	 * */
	public function it_sends_a_sms_when_contact_exists()
	{
		$contact = new Contact('andree', '999999999');
		$body = 'sms body';

		$contactRepository = Mockery::mock(ContactRepositoryInterface::class);
		$contactRepository->allows()->findByName('andree')->andReturns($contact);

		$provider = Mockery::mock(CarrierInterface::class);
		$provider->allows()->sms('')->andReturns(new Sms($contact, $body));
		$provider->allows()->validateNumber('999999999')->andReturns(true);

		$contactService = new ContactService($contactRepository);
		$callerService = new CallerService($provider);
		$smsService = new SmsService($provider);

		$mobile = new Mobile($contactService, $callerService, $smsService);

		$this->assertInstanceOf(Sms::class, $mobile->sendSmsByName('andree'));
		$this->assertEquals('Sending sms to contact andree with "sms body" to phone number 999999999', $mobile->sendSmsByName('andree')->send());
	}
}
