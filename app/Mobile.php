<?php

namespace App;

use App\Services\CallerService;
use App\Services\ContactService;
use App\Services\SmsService;

class Mobile
{

	private $callerService;
	private $contactService;
	private $smsService;

	function __construct(ContactService $contactService, CallerService $callerService, SmsService $smsService)
	{
		$this->callerService = $callerService;
		$this->contactService = $contactService;
		$this->smsService = $smsService;
	}


	/**
	 * make a phone call
	 */
	public function makeCallByName(string $name = ''): ?Call
	{
		if (empty(trim($name))) return null;

		$contact = $this->contactService->findByName($name);

		if (is_null($contact)) {
			return null;
		}

		$this->callerService->dial();

		return $this->callerService->makeCall();
	}

	/**
	 * send a sms
	 */
	public function sendSmsByName(string $name = '', string $body = ''): ?Sms
	{
		if (empty(trim($name))) return null;

		$contact = $this->contactService->findByName($name);

		if (is_null($contact)) {
			return null;
		}

		return $this->smsService->send($contact->phoneNumber(), $body);
	}
}
