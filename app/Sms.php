<?php

namespace App;

class Sms
{

    private $contact;
    private $body;

    public function __construct(Contact $contact, string $body)
    {
        $this->contact = $contact;
        $this->body = $body;
    }

    public function send(): string
    {
        return "Sending sms to contact {$this->contact->name()} with \"{$this->body}\" to phone number {$this->contact->phoneNumber()}";
    }
}
