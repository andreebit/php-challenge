<?php

namespace App\Providers;

use App\Call;
use App\Contact;
use App\Interfaces\CarrierInterface;
use App\Sms;

class CarrierTwoProvider implements CarrierInterface
{

    private $contact;

    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    public function dial(): string
    {
        return "Dialing {$this->contact->name()} from provider two";
    }

    public function call(): Call
    {
        return new Call($this->contact);
    }

    public function validateNumber(string $number): bool
    {
        return strlen($number) == 9;
    }

    public function contact(): Contact
    {
        return $this->contact;
    }

    public function sms(string $body): Sms
    {
        return new Sms($this->contact, $body);
    }
}
