<?php

namespace App;

class Call
{

	private $contact;

	function __construct(Contact $contact)
	{
		$this->contact = $contact;
	}

	public function start(): string
	{
		return "Calling to contact {$this->contact->name()} to phone number {$this->contact->phoneNumber()}";
	}
}
