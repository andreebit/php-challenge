<?php

namespace App\Repositories;

use App\Contact;
use App\Interfaces\ContactRepositoryInterface;

class ContactRepository implements ContactRepositoryInterface
{
    private $contacts = [];

    public function __construct()
    {
        // mock database rows
        $this->contacts = [
            new Contact('juan', '999999991'),
            new Contact('carlos', '999999992'),
            new Contact('pedro', '999999993'),
            new Contact('alberto', '999999994'),
            new Contact('roberto', '999999995'),
        ];
    }

    public function findByName(string $name): ?Contact
    {
        $foundElements = array_filter($this->contacts, function (Contact $contact) use ($name) {
            return ($contact->name() == $name);
        });

        return count($foundElements) > 0 ? $foundElements[array_key_first(($foundElements))] : null;
    }
}
