<?php

namespace App;

class Contact
{

	private $name;
	private $phoneNumber;

	function __construct(string $name, string $phoneNumber)
	{
		$this->name = $name;
		$this->phoneNumber = $phoneNumber;
	}

	public function name(): string
	{
		return $this->name;
	}

	public function phoneNumber(): string
	{
		return $this->phoneNumber;
	}
}
