<?php

use App\Contact;
use App\Interfaces\CarrierInterface;
use App\Providers\CarrierOneProvider;
use App\Providers\CarrierTwoProvider;

class ProviderFactory
{

    /**
     * Can you add support for two mobile carriers? How would you accomplish that?
     * 
     * Se puede crear un factory que retorne una implementación de la interfaz CarrierInterface
     * y así soportar cualquier empresa.
     * 
     */
    private $identifier;
    private $contact;
    public function __construct($identifier, Contact $contact)
    {
        $this->identifier = $identifier;
        $this->contact = $contact;
    }

    public function getInstance(): CarrierInterface
    {
        switch ($this->identifier) {
            case 1:
                return new CarrierOneProvider($this->contact);
                break;
            case 2:
                return new CarrierTwoProvider($this->contact);
                break;
            default:
                return new CarrierOneProvider($this->contact);
                break;
        }
    }
}
