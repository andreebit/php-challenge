<?php

namespace App\Interfaces;

use App\Call;
use App\Contact;
use App\Sms;

interface CarrierInterface
{

	public function dial(): string;

	public function call(): Call;

	public function validateNumber(string $number): bool;

	public function contact(): Contact;

	public function sms(string $body): Sms;
}