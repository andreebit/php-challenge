<?php

namespace App\Services;

use App\Call;
use App\Interfaces\CarrierInterface;

class CallerService
{

    private $provider;

    public function __construct(CarrierInterface $provider)
    {
        $this->provider = $provider;
    }

    public function dial(): string
    {
        return $this->provider->dial();
    }

    public function makeCall(): Call
    {
        return $this->provider->call();
    }

    public function validateNumber(string $number): bool
    {
        return $this->provider->validateNumber($number);
    }
}
