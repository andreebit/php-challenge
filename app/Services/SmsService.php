<?php

namespace App\Services;

use App\Interfaces\CarrierInterface;
use App\Sms;

class SmsService
{

    private $provider;

    public function __construct(CarrierInterface $provider)
    {
        $this->provider = $provider;
    }

    public function send(string $number, string $body): Sms
    {
        if ($this->provider->validateNumber($number)) {
            return $this->provider->sms($body);
        }
    }
}
