<?php

namespace App\Services;

use App\Contact;
use App\Interfaces\ContactInterface;
use App\Interfaces\ContactRepositoryInterface;

class ContactService
{

	protected $repository;

	public function __construct(ContactRepositoryInterface $repository)
	{
		$this->repository = $repository;	
	}

	public function findByName(string $name): ?Contact
	{
		return $this->repository->findByName($name);
	}
	
}
